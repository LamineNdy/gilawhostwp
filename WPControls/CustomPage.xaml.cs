﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace WPControls
{
    public partial class CustomPage : PhoneApplicationPage
    {
        public string PageId
        {
            get { return (string)GetValue(PageIdProperty); }
            set { SetValue(PageIdProperty, value); }
        }

        // Using a DependencyProperty as the backing store for PageId.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty PageIdProperty =
            DependencyProperty.Register("PageId", typeof(string), typeof(CustomPage), null);      
        
        public CustomPage()
        {
            InitializeComponent();
        }
    }
}
